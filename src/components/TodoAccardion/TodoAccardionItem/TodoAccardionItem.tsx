import MaximizeIcon from '@mui/icons-material/Maximize';
import { Switch, Typography, Box } from '@mui/material';
import "./tasksItem.css"
import { styled } from '@mui/material/styles';

interface TodoAccardionItemProps {
  checked: boolean;
  id: number;
  subtitle: string;
  title: string;
  setChecked: (id: number) => void;
}

const TodoAccardionItem: React.FC<TodoAccardionItemProps> = ({title, subtitle, checked, id, setChecked}) => {
  const IOSSwitch = styled((props) => (
    <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
  ))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
      padding: 0,
      margin: 2,
      transitionDuration: '300ms',
      '&.Mui-checked': {
        transform: 'translateX(16px)',
        color: '#fff',
        '& + .MuiSwitch-track': {
          backgroundColor: theme.palette.mode === 'dark' ? '#10C200' : '#10C200',
          opacity: 1,
          border: 0,
        },
        '& .MuiSwitch-thumb:before': {
            backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 16 16"><path fill="${encodeURIComponent(
              '#A9A9A9  ',
            )}" d="M4.11 2.697L2.698 4.11 6.586 8l-3.89 3.89 1.415 1.413L8 9.414l3.89 3.89 1.413-1.415L9.414 8l3.89-3.89-1.415-1.413L8 6.586l-3.89-3.89z"/></svg>')`,
          },
        '&.Mui-disabled + .MuiSwitch-track': {
          opacity: 0.5,
        },
      },
      '&.Mui-focusVisible .MuiSwitch-thumb': {
        color: '#A9A9A9',
        border: '6px solid #fff',
      },
      '&.Mui-disabled .MuiSwitch-thumb': {
        color:
          theme.palette.mode === 'light'
            ? theme.palette.grey[100]
            : theme.palette.grey[600],
      },
      '&.Mui-disabled + .MuiSwitch-track': {
        opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
      },
    },
    '& .MuiSwitch-thumb': {
      boxSizing: 'border-box',
      width: 22,
      height: 22,
      '&:before': {
        content: "''",
        position: 'absolute',
        width: '100%',
        height: '100%',
        left: 0,
        top: 0,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 24 24"><path fill="${encodeURIComponent(
          "#A9A9A9",
        )}" d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z" /></svg>')`,
      },
    },
    '& .MuiSwitch-track': {
      borderRadius: 26 / 2,
      backgroundColor: theme.palette.mode === 'light' ? '#366EFF' : '#366EFF',
      opacity: 1,
      transition: theme.transitions.create(['background-color'], {
        duration: 500,
      }),
    },
  }));


return (
    <Box className="tasksItem">
        <MaximizeIcon
            style ={{
                color: '#' + (Math.random().toString(16) + '000000').substring(2,8).toUpperCase(),
                transform: "rotate(90deg)",
                fontSize: "50px",
                borderRadius: "5px",
              }}
        />
        <Box className="item_flex">
            <Typography
              style={{ textDecoration: checked ? 'line-through' : undefined}}
              color="white"
              variant="h5"
            >
              {title}
            </Typography>
            <Typography style={{ opacity: '0.6'}} variant="subtitle2" color="white">{subtitle}</Typography>
        </Box>
        {/* @ts-ignore */}
        <IOSSwitch sx={{ m: 1 }} onClick={() => setChecked(id)} checked={checked} />
    </Box>
)
}

export default TodoAccardionItem;