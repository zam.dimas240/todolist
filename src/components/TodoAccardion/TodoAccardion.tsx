import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import TodoAccardionItem from './TodoAccardionItem/TodoAccardionItem';
import Checkbox from '@mui/material/Checkbox';
import { Box } from '@mui/material';
import './tasks.css'
import MaximizeIcon from '@mui/icons-material/Maximize';
import { todoItem } from '../../types/types';
import { Typography } from '@mui/material';

interface TodoAccardionProps {
  tasks: todoItem[];
  checked: boolean;
  id: number;
  setChekedAccardion: (id: number) => void;
  title: string;
  setChecked: (id: number) => void;
}


const TodoAccardion: React.FC<TodoAccardionProps> = ({ tasks, checked, id, setChekedAccardion, title, setChecked }) => {

  const allTaskscChanged = tasks.filter((el) => el.checked !== true);
 return (
    <Accordion style={{ background: '#282828', borderRadius: '40px', boxShadow: '16px 16px 20px rgba(0, 0, 0, 0.15), -8px -8px 20px rgba(255, 255, 255, 0.05)' }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon style={{ width: "21px", height: "21px", padding: "5px", color: "#000", backgroundColor: "#fff", borderRadius: "50%" }}  />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          className="accardion__summary"
          style={{ marginLeft: "-35px" }}
        >
          <MaximizeIcon
            style ={{
                color: '#' + (Math.random().toString(16) + '000000').substring(2,8).toUpperCase(),
                transform: "rotate(90deg)",
                fontSize: "50px",
                borderRadius: "5px",
              }}
        />
          <Checkbox checked={!allTaskscChanged.length || checked} 
                onChange={() => setChekedAccardion(id)}
                    style ={{
                        color: "#fff",
                      }}
                />
          <Typography
            style={{ textDecoration: !allTaskscChanged.length || checked ? 'line-through' : undefined}}
            color="white"
            variant="h5"
          >
            {title}
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
            <Box className="tasks__inline">
                {tasks.map((item) => (
                <TodoAccardionItem key={item.id} title={item.title} id={item.id} subtitle={item.subtitle} checked={checked || item.checked} setChecked={setChecked} />
              ))}
            </Box>
        </AccordionDetails>
    </Accordion>
  )
};

export default TodoAccardion;