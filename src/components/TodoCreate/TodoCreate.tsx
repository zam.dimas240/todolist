import { Button, TextField, Box } from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";
import { todoItem, todoList } from "../../types/types";

interface TodoCreateProps {
  createTodo: (item: todoList) => void;
}

const initialData = {
      id: 0,
      name: '',
      checked: false,
      tasks: [],
}

const initialDataTasks = 
      {
        id: 0,
        title: '',
        subtitle: '',
        checked: false,
      };

const TodoCreate: React.FC<TodoCreateProps> = ({ createTodo }) => {
  const [list, setList] = useState({ ...initialData, id: Math.random() });
  const [tasks, setTasks] = useState<todoItem[]>([{ ...initialDataTasks, id: Math.random() }]);

  useEffect(() => {
    //@ts-ignore
    setList({...list, tasks: tasks});
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tasks]);

  
  const created = () => {
    if (list.name) {
      //@ts-ignore
      createTodo(list);
      setTasks([{ ...initialDataTasks, id: Math.random() }]);
      setList({ ...initialData, id: Math.random() });
    }
  }

  const addPunkt = () => {
    const newQuestions = [...tasks, { ...initialDataTasks, id: Math.random() }];
    setTasks(newQuestions)
  }

  const changeTitle = (id: number, value: string) => {
    setTasks(prevState => 
      prevState.map(item => 
        item.id === id 
          ? { ...item, title: value }
          : item
      )
    )
  }

  const removePunkt = () => {
    const removed = tasks.splice(-1,1);
    setTasks(removed);
  }

  const changeSubtitle = (id: number, value: string) => {
    setTasks(prevState => 
      prevState.map(item => 
        item.id === id 
          ? { ...item, subtitle: value }
          : item
      )
    )
  }
  return (
    <Box style={{ marginBottom: '20px'}}>
      <TextField value={list.name} style={{ background: '#fff', marginBottom: '10px'}} id="filled-basic" label="На какой день" onChange={(e) => setList({...list, name: e.target.value})} variant="filled" />
      {tasks.map((item, ind) => (
        <Box key={item.id} style={{ marginBottom: '10px', paddingBottom: '10px', borderBottom: '4px solid yellow'}}>
          <TextField
            style={{ background: '#fff', marginBottom: '10px' }}
            id="filled-basic"
            value={tasks[ind].title}
            label="Название"
            onChange={(e) => changeTitle(item.id, e.target.value)} 
            variant="filled" />
          <TextField
            style={{ background: '#fff'}}
            id="filled-basic"
            value={tasks[ind].subtitle}
            onChange={(e) => changeSubtitle(item.id, e.target.value)}
            label="Описание"
            variant="filled" />
        </Box>
      ))}
      <Box display='flex' justifyContent='center' mb={1}>
        <Box>
          <Button onClick={() => addPunkt()} variant="outlined">Добавить пункт</Button>
        </Box>
        <Box>
          <Button onClick={() => removePunkt()} variant="outlined">Удалить пункты</Button>
        </Box>
      </Box>
      <Button onClick={() => created()} variant="contained">Добавить</Button>
    </Box>
  );
};

export default TodoCreate;