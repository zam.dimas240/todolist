import axios from "axios";

const apiUrl = 'https://newsdata.io/api/1';
const apiKey = 'pub_12145fa75e7d80c7df3080bde6aa36551bc66';

axios.defaults.baseURL = apiUrl;

export const newsAgent = {
  async getAll() {
    return axios.get('/news', { params: { apikey: apiKey, q: 'Russia'}});
  }
}