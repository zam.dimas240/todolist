export interface todoList {
  id: number,
  name: string,
  checked: boolean,
  tasks: [todoItem]
}

export interface todoItem {
  id: number,
  title: string,
  subtitle: string,
  checked: boolean
}