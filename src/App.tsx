/* eslint-disable jsx-a11y/no-distracting-elements */
/* eslint-disable react/jsx-no-comment-textnodes */
import { useState } from 'react';
import { newsAgent } from './agents/newsAgent';
import { Modal, Switch, FormControlLabel, Box } from '@mui/material';
import './App.css';
import TodoAccardion from './components/TodoAccardion/TodoAccardion';
import TodoCreate from './components/TodoCreate/TodoCreate';
import SettingsIcon from '@mui/icons-material/Settings';
import { todoList } from './types/types';
import { useQuery } from 'react-query';
import { Typography } from '@mui/material';

function App() {
  const {isLoading} = useQuery('news', () => newsAgent.getAll(), {
    onSuccess: ({ data }) => {
      setNews(data.results);
    }
  })

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [todoList, setTodoList] = useState<todoList[]>([]);
  const [news, setNews] = useState();
  const [switchNews, setSwitchNews] = useState(true);

  const createTodo = (item: todoList) => {
    setTodoList([...todoList, item]);
  }

  const setChecked = (id: number) => {
    const newTodoList = todoList.map(item => {
      return {...item, tasks: item.tasks.map(el => {
       return el.id === id 
        ? { ...el, checked: !el.checked }
        : el
      })}
    })
    //@ts-ignore
    setTodoList(newTodoList)
  }

  const setChekedAccardion = (id: number) => {
    const newTodoList = todoList.map(item => 
       item.id === id 
        ? { ...item, checked: !item.checked }
        : item
      )

    setTodoList(newTodoList)
  }

  return (
    <>
    <Box className="App">
      <Box className="Header">
            <Typography variant="h4" color="white" fontWeight="bold">To Do</Typography>
            <SettingsIcon onClick={() => handleOpen()} style={{ color: 'white' }}/>
        </Box>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        style={{ display: "flex", justifyContent: "center", alignItems: "center" }}
      >
          <FormControlLabel control={<Switch checked={switchNews} onChange={() => setSwitchNews(!switchNews)} />} label="Включить/Выключить новости" />
      </Modal>
      <TodoCreate createTodo={createTodo} />
      {todoList.map((item) => (
        <TodoAccardion key={item.id} title={item.name} id={item.id} checked={item.checked} setChekedAccardion={setChekedAccardion} tasks={item.tasks} setChecked={setChecked}/>
      ))}
    </Box>
    {((switchNews && news) || isLoading) && (
        <Box className="container">
          <Box className="ticker-box">
            {isLoading ? <h2>Loading...</h2> : (
              <>
            <Box className="ticker-title">
              News
            </Box>
              {/* @ts-ignore */}
              <marquee id="ticker" className="ticker-body">
                {/* @ts-ignore */}
                {news && switchNews && news.map((el: any) => (
                  <span>{el.title}</span>
                ))}
              {/* @ts-ignore */}
              </marquee>
              </>
            )}
          </Box>
        </Box>
      )}
    </>
  );
}

export default App;
